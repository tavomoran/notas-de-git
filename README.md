# Notas de Git

Aprendiendo a utilizar git


#Git global setup

git config --global user.name "kal el"

git config --global user.email "super@krypton.sup"

git config --global core.editor "vim"

git config --list

git log --al --decorate --graph --oneline

#Configurar llaves ssh

ssh-keygen -t rsa -C "super@krypton.sup"

Se elige la ruta y el nombre para el archivo
Si se desea se elige una frase de paso, pero se pedira cada vez que se haga un push, se puede dejar vacía

#Create a new repository

git clone git@gitlab.com:tavomoran/notas-de-git.git

cd notas-de-git

touch README.md

git comit -m "add README"

git push -u origin master


#Existing folder for Git repository

cd existing_folder

git init

git remote add origin git@gitlab.com:tavomoran/notas-de-git.git

git add

git commit

git push - u origin master


#Saber el stattus

git status

#Visualizar los respositorios a los que estamos asociados

git remote -v

#Asociar un repositorio

git remote add origin git@gitlab.com:tavomoran/notas-de-git.git

#Desasociar un repositorio

git remote remove 

# Crear una Rama
git branch nombreRama

# Listar Ramas
git branch

# Cambiar de Rama
git checkout nombreRama


